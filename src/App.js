import { BrowserRouter as Router, Switch, Route } from "react-router-dom";

import Startpage from "./pages/Start";
import Intro from "./pages/Intro";
import Fallback from "./pages/404";

export default function App() {
  return (
    <div className="App">
      <Router>
        <Switch>
          <Route exact path="/">
            <Startpage />
          </Route>
          <Route path="/intro">
            <Intro />
          </Route>
          <Route path="*">
            <Fallback />
          </Route>
        </Switch>
      </Router>
    </div>
  );
}
