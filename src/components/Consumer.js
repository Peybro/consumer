export default function Consumer({amount}) {
    return <>{`Consumer/kənˈsjuːmə/`.repeat(amount)}</>;
}
