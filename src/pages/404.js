import { Link } from "react-router-dom";
import { Helmet } from "react-helmet";

export default function Fallback() {
  return (
    <>
      <Helmet>
        <title>404</title>
        <meta name="description" content="Nothing to see here. Go away." />
      </Helmet>
      <p>Nothing to see here... Go.</p>
      <Link to="/">Go back</Link>
    </>
  );
}
