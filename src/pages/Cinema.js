import { useEffect, useState } from "react";
import Cookies from "js-cookie";
import { Helmet } from "react-helmet";
import styled from "styled-components";

import Video from "./../components/Video";
import Back from "./../components/Back";

const videos = [
  {
    name: "Ephemeral Device",
    src: "https://www.youtube.com/embed/JmkXcFBG1OA",
  },
];

//styles
const CinemaPage = styled.div`
  display: grid;
  grid-template-columns: auto;
  row-gap: 10px;
`;

const CookieInfo = styled.div`
  background: #fff;
  width: 90vw;
  padding: 5px 10px;
  max-width: 800px;
`;

const CookieCheckbox = styled.div`
  margin: 10px 0;
`;

const OkBtn = styled.button`
  padding: 5px 10px;
  cursor: pointer;
  float: right;
  background: yellow;
`;

const VideoList = styled.div`
  max-height: 80vh;
  overflow-y: scroll;
  display: grid;
  row-gap: 20px;
`;

export default function Cinema() {
  const [showVideos, setShowVideos] = useState(false);
  const [rememberDecision, setRememberDecision] = useState(false);

  useEffect(() => {
    if (Cookies.get("showVideos") === "true") setShowVideos(true);
  }, []);

  function handleShowVideos() {
    if (rememberDecision) {
      Cookies.set("showVideos", "true", { expires: 30, path: "" });
    }
    setShowVideos(true);
  }

  return (
    <CinemaPage>
      <Helmet>
        <title>Cinema</title>
        <meta name="description" content="Watch Movies. Enjoy." />
      </Helmet>
      <Back to="/intro" />
      {!showVideos && (
        <CookieInfo>
          <h1>
            Loading these videos will make YouTube safe cookies in your browser
          </h1>
          <CookieCheckbox>
            <input
              type="checkbox"
              checked={rememberDecision}
              onChange={() => setRememberDecision(!rememberDecision)}
              id="rememberme"
            />
            <label htmlFor="rememberme">
              {" "}
              remember my decision (will set an additional cookie)
            </label>
          </CookieCheckbox>
          <OkBtn onClick={() => handleShowVideos()}>Ok load them</OkBtn>
        </CookieInfo>
      )}
      <VideoList>
        {showVideos &&
          videos.map((video) => <Video key={video.src} src={video.src} />)}
      </VideoList>
    </CinemaPage>
  );
}
