import { Switch, Route } from "react-router-dom";
import { Helmet } from "react-helmet";
import styled from "styled-components";

import Consumer from "../components/Consumer";
import CategoryCard from "../components/CategoryCard";
import Shop from "./Shop";
import Cinema from "./Cinema";

//styles
const IntroPage = styled.div`
  height: 100vh;
  width: 100vw;
  overflow: hidden;
  display: flex;
  justify-content: center;
  align-items: center;
  position: relative;

  @media screen and (max-width: 320px) {
    overflow-y: scroll;
    align-items: initial;
  }
`;
const IntroContent = styled.div`
  z-index: 10;

  @media screen and (max-width: 320px) {
    margin-top: 20px;
  }
`;
const IntroBackground = styled.div`
  font-style: italic;
  position: absolute;
  word-break: break-all;
`;
const CardContainer = styled.div`
  display: grid;
  grid-column-gap: 50px;
  row-gap: 50px;

  @media screen and (max-width: 320px) {
    grid-template-columns: auto;
    row-gap: 20px;
  }

  @media screen and (min-width: 720px) {
    grid-template-columns: auto auto;
  }
`;

export default function Intro() {
  // const deviceHeight = window.innerHeight;
  const deviceWidth = window.innerWidth;

  return (
    <IntroPage>
      <Helmet>
        <title>Overview</title>
        <meta name="description" content="Do this or that." />
      </Helmet>
      <IntroBackground>
        {
          // iPhone 5 suitable
          deviceWidth <= 320 && <Consumer amount={100} />
        }
        {
          // actually for 4K screens
          deviceWidth > 320 && <Consumer amount={4000} />
        }
      </IntroBackground>

      <IntroContent>
        <Switch>
          <Route exact path="/intro">
            <CardContainer>
              <CategoryCard
                title={"Shopping"}
                to={"/intro/shop"}
                img={{
                  alt: "cart",
                  src:
                    "https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Fdijf55il5e0d1.cloudfront.net%2Fimages%2Fna%2F3%2F7%2F4%2F37497_1000.jpg&f=1&nofb=1",
                }}
              />
              <CategoryCard
                title={"Cinema"}
                to={"/intro/cinema"}
                img={{
                  alt: "cinema",
                  src:
                    "https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Fc.pxhere.com%2Fphotos%2Ff5%2Fbe%2Fcolumbus_ohio_ohio_theatre_theater_marquee_front_entrance_downtown-1206124.jpg!d&f=1&nofb=1",
                }}
              />
            </CardContainer>
          </Route>
          <Route exact path="/intro/shop">
            <Shop />
          </Route>
          <Route exact path="/intro/cinema">
            <Cinema />
          </Route>
        </Switch>
      </IntroContent>
    </IntroPage>
  );
}
